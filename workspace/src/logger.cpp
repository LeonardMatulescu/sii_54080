#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

typedef struct mensaje{
	int jugador;
	int puntos;
}mensaje;

int main(int argc, char **argv){
	int fd, puntos;
	
	if(argc != 1){
		fprintf(stderr, "Uso: %s logger\n", argv[0]);
		return 1;
	}
	
	//Creamos el FIFO
	if(mkfifo("FIFO", 0600) < 0){
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	//Abrimos el FIFO
	if ((fd = open("FIFO", O_RDONLY)) < 0){
		perror("No puede abrirse el FIFO");
		return 1;
	}
	mensaje m;
	while((read(fd, &m, sizeof(m))) && m.puntos != 3){
		printf("Jugador %d marca 1 punto, lleva un total de %d puntos.\n", m.jugador, m.puntos);
		
	}
	printf("3 puntos alcanzados, Ha ganado el jugador %d, ¡¡¡Enhorabuena!!!\n", m.jugador);
	
	close(fd);
	unlink("FIFO");
	return 0;
}